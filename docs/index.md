---
layout: home

hero:
    name:    nano/modules/security
    tagline: security module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/security is a security module for nano
    -   title:   security
        details: |
                 nano/modules/security provides minimal security features for nano
---
