---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-security
```

## registering the module

```php
use laylatichy\nano\modules\security\SecurityModule;

useNano()->withModule(new SecurityModule());
```

