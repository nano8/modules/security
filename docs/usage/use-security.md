---
layout: doc
---

<script setup>
const args = {
    useSecurity: [],
};
</script>

## usage

use the `useSecurity` function to get security related objects


## <Types fn="useSecurity" r="Security" :args="args.useSecurity" /> {#useSecurity}

```php
useSecurity();
```


