---
layout: doc
---

<script setup>
const args = {
    useSecurity: [],
    hash: [
        { 
            type: 'string',
            name: 'password',
        },
    ],
    generate: [
        { 
            type: 'int',
            name: 'length',
        },
    ],
    verify: [
        { 
            type: 'string',
            name: 'password',
        },
        { 
            type: 'string',
            name: 'hash',
        },
    ],
};
</script>

# password

## usage

## <Types fn="hash" r="string" :args="args.hash" /> {#hash}

```php
useSecurity()->password->hash($password);
```

## <Types fn="generate" r="string" :args="args.generate" /> {#generate}

```php
useSecurity()->password->generate($length);
```

## <Types fn="verify" r="bool" :args="args.verify" /> {#verify}

```php
useSecurity()->password->verify($password, $hash);
```


