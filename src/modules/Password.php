<?php

namespace laylatichy\nano\modules\security\modules;

class Password {
    public function __construct() {
        // nothing to do here
    }

    public function hash(string $password): string {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function generate(int $length = 10): string {
        do {
            $string = preg_replace(
                '/[^a-zA-Z0-9]/',
                '',
                base64_encode($this->getRandomBytes($length + 1))
            );
        } while (!$string);

        return substr($string, 0, $length);
    }

    public function verify(string $password, string $hash): bool {
        return password_verify($password, $hash);
    }

    private function getRandomBytes(int $nbBytes = 32): string {
        do {
            $bytes = openssl_random_pseudo_bytes($nbBytes, $strong);
        } while (!$bytes || !$strong);

        return $bytes;
    }
}
