<?php

namespace laylatichy\nano\modules\security;

use laylatichy\nano\modules\security\modules\Password;

class Security {
    public function __construct(
        public readonly Password $password = new Password(),
    ) {
        // nothing to do here
    }
}
