<?php

namespace laylatichy\nano\modules\security;

use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

class SecurityModule implements NanoModule {
    public Security $security;

    public function __construct() {
        // nothing to do here
    }

    public function register(Nano $nano): void {
        if (isset($this->security)) {
            useNanoException('security module already registered');
        }

        $this->security = new Security();
    }
}
