<?php

use laylatichy\nano\modules\security\Security;
use laylatichy\nano\modules\security\SecurityModule;

if (!function_exists('useSecurity')) {
    function useSecurity(): Security {
        return useNanoModule(SecurityModule::class)
            ->security;
    }
}